﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoistWwiseEvent : MonoBehaviour
{
    public string Footstep = "Footstep";
    public AK.Wwise.Event FootstepEvent;

    void Start()
    {

        AkSoundEngine.RegisterGameObj(gameObject);
    }

   public void PlayFootstep()
    {
        FootstepEvent.Post(gameObject);
      
    }
}
